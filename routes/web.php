<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contact-us/{english?}', [PageController::class, 'contact'])->name('contact');
Route::get('/about-us/{english?}', [PageController::class, 'about'])->name('about');
Route::get('/portfolio/{english?}', [PageController::class, 'portfolio'])->name('portfolio');
Route::get('/{english?}', [PageController::class, 'home'])->name('home');

Route::post('/contact-form/{english?}', [Controller::class, 'contactForm'])->name('contact-form');
