@extends('layout')

@section('title', '')

@section('page-style')
  <link rel="stylesheet" href="{{asset('css/home.css')}}">
@endsection

@section('content')
  <div class="home">
    <div class="heading @if ($eng) heading-eng @endif">
      <div class="container">
        <h1>@if ($eng) Adorian Web Development @else توسعه وب آدُریان @endif</h1>
        <p>@if ($eng) We produce your custom software or website for your personal use or for your website at Adorian. Contact us to begin. @else ما در آدُریان نرم افزار سفارشی یا وبسایت مورد نیاز شخصی شما یا کسب و کار شما را تولید می کنیم. برای شروع با ما تماس بگیرید. @endif</p>
        <a class="link" href="{{route('contact')}}">@if ($eng) Contact Us @else تماس با ما @endif</a>
      </div>
    </div>

    <div class="container">
      <div class="about">
        <h1>@if ($eng) Helping Businesses and Individuals @else کمک به کسب و کارها و افراد @endif</h1>
        <p>@if ($eng) Attracting and keeping customers requires giving people the information they want to know about your business. If your business does not have a proper website then new customers may not give your business a try. You could even lose current customers to a competiter. Also many customers find it easier to access your services through applications and websites. We are here to help you with all that. @else جذب کردن و نگه داشتن مشتری نیازمند این است که به مردم اطلاعاتی را که می خواهند بدهید. اگر شما یک وبسایت مناسب نداشته باشید، مشتریان جدید ممکن است کسب و کار شما را حتی امتحان نکنند یا ممکن است مشتری های فعلی خودتان را به رقیبتان ببازید. همچنین برای بسیاری از مشتریان آسان تر است که خدمات شما را از طریق اپلیکیشن یا وبسایت دریافت کنند. ما اینجا هستیم تا در تمام این موارد به شما کمک کنیم. @endif</p>
        <a class="link" href="{{route('about')}}">@if ($eng) More About Us & Our Services @else بیشتر درباره ما و خدمات ما @endif</a>
      </div>

      <div class="portfolio">
        <h1>@if ($eng) Examples of Our Work @else نمونه هایی از کار ما @endif</h1>

        <div class="works">
          <div class="work">
            <img src="{{asset('images/traderjournal.png')}}" alt="Trader Journal Image">
            <h3>@if ($eng) Trader Journal @else ژورنال معامله گر @endif</h3>
          </div>
          <div class="work">
            <img src="{{asset('images/imagebay.png')}}" alt="ImageBay Image">
            <h3>ImageBay</h3>
          </div>
          <div class="work">
            <img src="{{asset('images/resume.png')}}" alt="Resume Image">
            <h3>@if ($eng) Online Resume @else رزومه آنلاین @endif</h3>
          </div>
          <div class="work">
            <img src="{{asset('images/contractor-map.png')}}" alt="Contractor Map Image">
            <h3>@if ($eng) Contractors Map @else نقشه پیمانکاران @endif</h3>
          </div>
        </div>

        <a href="{{route('portfolio')}}" class="link">@if ($eng) View Our Gallery @else از گالری ما دیدن کنید @endif</a>
      </div>
    </div>
  </div>
@endsection
