<!DOCTYPE html>
<html @if ($eng) lang="en" @else lang="fa" @endif>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@if ($eng) Adorian @else آدُریان @endif @yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    @if ($eng) <link rel="stylesheet" href="{{asset('css/layout-en.css')}}"> @endif
    @yield('page-style')
</head>
<body @if ($eng) dir="ltr" @else dir="rtl" @endif>
    <div class="layout">
        <header class="header">
            <div class="container">
                <div class="header__phone-menu" id="phone-menu"><div></div><div></div><div></div></div>
                <a class="header__title" href="{{route('home')}}">
                    <img src="{{asset('images/logo.png')}}" alt="logo">
                    <h1>@if ($eng) Adorian @else آدُریان @endif</h1>
                </a>
                <nav class="header__menu" id="main-menu">
                    <i class="fas fa-arrow-right" id="close-btn"></i>
                    <a href="{{route('home')}}">@if ($eng) Main Page @else صفحه اصلی @endif</a>
                    <a href="{{route('about')}}">@if ($eng) About US @else درباره ما @endif</a>
                    <a href="{{route('portfolio')}}">@if ($eng) Portfolio @else نمونه کارها @endif</a>
                    <a href="{{route('contact')}}">@if ($eng) Contact US @else  تماس با ما @endif</a>
                </nav> 
                <div class="header__lang">
                    <i class="fas fa-globe"></i>
                    @if ($eng)
                        <a href="{{URL::current() . '?lang=fa'}}">فارسی</a>
                    @else
                        <a href="{{URL::current() . '?lang=en'}}">English</a>
                    @endif
                </div>           
            </div>
        </header>

        <main class="main">
            @yield('content')
        </main>

        <footer class="footer">
            <div class="container">
                <div class="footer__about">
                    <h2>@if ($eng) About Us @else درباره ما @endif</h2>
                    <p>@if ($eng) We are a web development technology company. Our goal is to help your customers get their customized computer software needs as easy as possible. Contact us today to schedule an appointment to discuss your new website or application. @else ما یک شرکت در زمینه تکنولوژی برنامه نویسی وب هستیم. هدف ما این است که مشتریانمان به راحتترین شکل ممکن نیازهای سفارشی نرم افزاری خود را برطرف کنند. امروز با ما تماس بگیرید تا درباره وبسایت یا اپلیکیشن جدید شما صحبت کنیم. @endif</p>
                </div>

                <div class="footer__contact">
                    <h2>@if ($eng) Contact Us @else تماس با ما @endif</h2>
                    <a href="tel:00989358364733">
                        <i class="fas fa-mobile-alt"></i>
                        @if ($eng) +989358364733 @else 09358364733 @endif
                    </a>
                    <a href="mailto:contact@adorian.ir">
                        <i class="fas fa-at"></i>
                        contact@adorian.ir
                    </a>
                    <a href="https://t.me/adorian_ir">
                        <i class="fab fa-telegram"></i>
                        @if ($eng) Telegram @else تلگرام @endif
                    </a>
                </div>
            </div>
        </footer>

        <div id="backdrop"></div>
    </div>

    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>