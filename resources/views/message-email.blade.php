<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>email from company website</title>
</head>
<body>
  <h3>خانم/آقای {{$data['name']}}</h3>
  <p>ایمیل: {{$data['email']}}</p>
  <p>پیام: {{$data['message']}}</p>
</body>
</html>