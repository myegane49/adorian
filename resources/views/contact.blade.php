@extends('layout')

@if ($eng) @section('title', ' | Contact Us') @else @section('title', ' | تماس با ما') @endif

@section('page-style')
  <link rel="stylesheet" href="{{asset('css/contact.css')}}">
@endsection

@section('content')
  <div class="contact @if ($eng) contact-eng @endif">
    <div class="container">
      <h1 class="contact__heading">@if ($eng) Contact Us Today To Get Started @else برای شروع امروز با تماس بگیرید @endif</h1>
      <p class="contact__subHead">@if ($eng) Thank you for visiting our website. Please contact us through the methods below. We look forward to being of assistance. @else از اینکه از وبسایت ما دیدن کردید سپاس گزاریم. لطفا از طریق روش های زیر با ما تماس بگیرید. خوشحال می شویم به شما کمک کنیم. @endif</p>

      <div class="contact__methods">
        <form class="contact__form @if ($eng) contact-eng__form @endif" method="POST" @if ($eng) action="{{route('contact-form', ['english' => 'en'])}}" @else action="{{route('contact-form')}}" @endif>
          @csrf
          <h2>@if ($eng) Contact Form @else فرم تماس @endif</h2>
          @if (session('sentMessage')) 
            <p class="sent-message">{{session('sentMessage')}}</p>
          @endif
          @if (session('errorSending')) 
            <p class="error-sending">{{session('errorSending')}}</p>
          @endif
          
          <div class="form-control">
            <label>@if ($eng) First Name and Last Name @else نام و نام خانوادگی @endif</label>
            <input type="text" name="name">
          </div>
          @error('name')
            <div class="field-error">{{$message}}</div>  
          @enderror
          <div class="form-control">
            <label>@if ($eng) Email @else ایمیل @endif</label>
            <input type="text" name="email" dir="ltr">
          </div>
          @error('email')
            <div class="field-error">{{$message}}</div>  
          @enderror
          <div class="form-control">
            <label>@if ($eng) Message @else پیام @endif</label>
            <textarea name="message"></textarea>
          </div>
          @error('message')
            <div class="field-error">{{$message}}</div>  
          @enderror

          <button>@if ($eng) Send @else فرستادن @endif</button>
        </form>

        <div class="contact__addresses @if ($eng) contact-eng__addresses @endif">
          <h2>@if ($eng) Other Contact Methods @else راه های دیگر تماس @endif</h2>
          <a href="tel:00989358364733">
            <i class="fas fa-mobile-alt"></i>
            @if ($eng) 00989358364733 @else 09358364733 @endif
          </a>
          <a href="mailto:contact@adorian.ir">
              <i class="fas fa-at"></i>
              contact@adorian.ir
          </a>
          <a href="https://t.me/adorian_ir">
              <i class="fab fa-telegram"></i>
              @if ($eng) Telegram @else تلگرام @endif
          </a>
        </div>
      </div>
    </div>
  </div>
@endsection
