@extends('layout')

@if ($eng) @section('title', ' | About Us') @else @section('title', ' | درباره ما') @endif

@section('page-style')
  <link rel="stylesheet" href="{{asset('css/about.css')}}">
@endsection

@section('content')
  <div class="about">
    <div class="container">
      <h1 class="about__heading">@if ($eng) What we build in Adorian @else در آدُریان چه می سازیم @endif</h1>
      <p class="about__briefIntro">@if ($eng) To be concise we build anything that revolves around web technologies from Wordpress websites to web applications to PWAs to even desktop applications built with Electronjs for all the operating systems. At Adorian we don't want to limit ourselves to a specific niche so we can cover any software plan you might need. We respect your privacy and freedom. that is why we don't develop software for operating systems like IOS or Android or MacOS which are literally jails and we suggest you not to use them at all or at least limit using them as much as you can. At the same time we understand how the new world is so dependant on these platforms so we decided to at least include PWAs in our services. below you can see why a PWA is better than an Android or IOS native application. @else  به طور خلاصه ما هر چیزی که در ارتباط با تکنولوژی وب باشد میسازیم. از وبسایت وردپرسی گرفته تا وب اپلیکیشن تا PWA تا حتی نرم افزار دسکتاپ با استفاده از Electronjs برای همه سیستم عامل ها. در آدُریان ما نمی خواهیم خودمان را به یک نوع بیزینس خاص محدود کنیم چون می خواهیم برای هر نیاز نرم افزاری شما پاسخگو باشیم. ما به آزادی و حریم خصوصی شما احترام می گذاریم به همین علت برای سیستم عامل های IOS و Android و MacOS نرم افزار تولید نمی کنیم و به شما پیشنهاد می کنیم از آنها استفاده نکنید یا حداقل تا جایی که می توانید استفاده از آنها را محدود کنید. اگرچه، ما وابستگی شدید جهان مدرن را به این پلتفورم ها درک می کنیم و به همین دلیل PWA را نیز جزو خدمات و تولیداتمان قرار دادیم. در پایین چرایی برتری یک PWA را نسبت به اپلیکیشن Android و IOS می بینید. @endif</p>

      <div class="about__pwaProof">
        <h1>@if ($eng) Studies Shows: @else مطالعات و پژوهش ها نشان می دهد که: @endif</h1>
        <ul>
          <li>@if ($eng) How many new app does the average user install per month on his/her phone? the answer is zero @else یک کاربر متوسط چه تعداد اپلیکیشن جدید در ماه بر روی موبایل خود نصب می کند؟ پاسخ صفر است. @endif</li>
          <li>@if ($eng) 80% of time spent happens in user's top 3 apps which are probably instagram, telegram, ... . so your app gets no time @else ۸۰٪ زمان گذرانده شده توسط کاربر روی ۳ اپلیکیشن برتر گوشی خود است که احتمالا اینستاگرام، تلگرام، ... است. پس هیچ زمانی برای اپلیکیشن شما صرف نمی شود. @endif</li>
          <li>@if ($eng) we can reach a broader audience in mobile web even though the time spent is low. so people do spend a lot of time on native apps but it is the top 3 apps. other native apps don't get many visitors. it is easier to get to web page, you just search it and it's there but for native apps you need to install them first @else در وب موبایل به گستره مخاطب بیشتری دسترسی داریم اگرچه زمان کمتری برای آنها صرف می شود. درست است که مردم زمان زیادی را برای اپ های موبایل صرف می کنند ولی این زمان فقط برای ۳ اپ برتر گوشی آنهاست و از اپ های دیگر زیاد بازدید نمی شود. دسترسی به یک صفحه وب بسیار آسان تر است کافیست در موتورهای جستجو دنبال آن بگردید ولی اپ های موبایل را ابتدا باید نصب کرد. @endif</li>
        </ul>

        <div class="graphs">
          <div class="audience">
            <div class="bars">
              <div>
                <div class="column">
                  <h4>@if ($eng) 3.3 million people per month @else ۳/۳ میلیون نفر در ماه @endif</h4>
                  <div class="app-bar"></div>
                </div>
                <h4>@if ($eng) Mobile App @else اپ موبایل @endif</h4>
              </div>
              <div>
                <div class="column">
                  <h4>@if ($eng) 8.9 million people per month @else 8.9 میلیون نفر در ماه @endif</h4>
                  <div class="mobile-bar"></div>
                </div>
                <h4>@if ($eng) Mobile Web @else وب موبایل @endif</h4>
              </div>
            </div>
          </div>

          <table class="features">
            <thead>
              <tr>
                <th></th>
                <th>@if ($eng) Capability @else قابلیت @endif</th>
                <th>@if ($eng) Audience @else مخاطب @endif</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="title">@if ($eng) Mobile App @else اپ موبایل @endif</td>
                <td class="has">@if ($eng) Access Device Features and OS @else دسترسی به ویژگی های گوشی و سیستم عامل @endif</td>
                <td class="hasnot">@if ($eng) Top 3 Apps Win, Rest Loses @else ۳ اپ برتر می برند بقیه می بازند @endif</td>
              </tr>
              <tr>
                <td class="title">@if ($eng) Web App @else وب اپ @endif</td>
                <td class="hasnot">@if ($eng) Highly Limited Device Features Access @else دسترسی خیلی محدود به ویژگی های گوشی @endif</td>
                <td class="has">@if ($eng) Large Audeince @else مخاطب زیاد @endif</td>
              </tr>
              <tr>
                <td class="title">PWA</td>
                <td class="has">@if ($eng) Access Device Features and OS @else دسترسی به ویژگی های گوشی و سیستم عامل @endif</td>
                <td class="has">@if ($eng) Large Audeince @else مخاطب زیاد @endif</td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>

      <div class="about__pricing">
        <h1>@if ($eng) Price Range @else محدوده قیمت @endif</h1>
        <ul>
          <li>@if ($eng) Starting at $450 with larger products requiring a custom quote. @else شروع قیمت از 400 دلار و برای پروژه های بزرگتر قیمت بیشتری تعیین می شود. @endif</li>
          <li>@if ($eng) Any probable existing bug or error will be fixed free of charge. @else هر گونه باگ یا خطای احتمالی به صورت رایگان رفع می شود. @endif</li>
          <li>@if ($eng) Our support for your website lasts for one year. @else پشتیبانی ما برای وبسایت شما یک ساله می باشد. @endif</li>
        </ul>
      </div>

      <div class="about__personnel">
        <div class="intro">
          <h1>@if ($eng) Developer @else برنامه نویس @endif</h1>
          <p>@if ($eng) I am Mohammad Ahmady Yeganeh. Freelance web developer and the owner of this brand. I do all the work around here from front-end development to server administration and marketing for now. @else من محمد احمدی یگانه هستم. برنامه نویس وب فریلنس و صاحب این برند. فعلا تمام کارهای اینجا با من هست از برنامه نویسی سمت فرانت گرفته تا مدیریت سرور و بازاریابی. @endif</p>
          <a href="https://myegane49.github.io/" target="_blank" class="link">@if ($eng) Resume @else رزومه @endif</a>
        </div>
        <img src="{{asset('images/photo.jpg')}}">
      </div>
    </div>
  </div>
@endsection
