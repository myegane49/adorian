@extends('layout')

@if ($eng) @section('title', ' | Portfolio') @else @section('title', ' | نمونه کارها') @endif

@section('page-style')
  <link rel="stylesheet" href="{{asset('css/portfolio.css')}}">
@endsection

@section('content')
  <div class="portfolio">
    <div class="container">
      <h1 class="portfolio__heading">@if ($eng) Some Examples Of Our Work @else چند نمونه از کارهای ما @endif</h1>
      <p class="portfolio__subHead">@if ($eng) You can see examples of our work below. Contact us today to begin. @else تعدادی از کارهای ما را در پایین می بینید. امروز با ما تماس بگیرید تا شروع کنیم @endif</p>
   
      <div class="work">
        <div>
          <h2>@if ($eng) Trader Journal @else ژورنال معامله گر @endif</h2>
          <p>@if ($eng) An application built with Laravel and Vuejs to register trades of a financial markets trader to use them for risk management and generating report @else اپلیکیشنی برای ثبت تریدها در بازارهای مالی و استفاده از آن برای تهیه گزارش عملکرد و مدیریت ریسک ساخته شده با Laravel و Vuejs @endif</p>
          <a href="https://www.traderjournal.ir" target="_blank">@if ($eng) Visit Website @else دیدن وبسایت @endif</a>
        </div>
        <img src="{{asset('images/traderjournal.png')}}">
      </div>

      <div class="work">
        <div>
          <h2>@if ($eng) Personal Website @else وبسایت شخصی @endif</h2>
          <p>@if ($eng) A personal website. A SSR built with Nuxtjs with Mongodb Database. @else یک وبسایت شخصی. یک SSR ساخته شده با Nuxtjs و دیتابیس Mongodb @endif</p>
          <a href="https://myegane49.xyz" target="_blank">@if ($eng) Visit Website @else دیدن وبسایت @endif</a>
        </div>
        <img src="{{asset('images/myegane49_xyz.png')}}">
      </div>

      <div class="work">
        <div>
          <h2>ImageBay</h2>
          <p>@if ($eng) A Nodejs CMS with Mongodb database for sharing images and using them on image editing projects @else یک CMS با استفاده از Nodejs و دیتابیس Mongodb برای به اشتراک گذاری تصاویر و استفاده در پروژه های ویرایش تصاویر. @endif</p>
          <a href="https://imagebay.ir" target="_blank">@if ($eng) Visit Website @else دیدن وبسایت @endif</a>
        </div>
        <img src="{{asset('images/imagebay.png')}}">
      </div>

      <div class="work">
        <div>
          <h2>@if ($eng) Online Resume @else رزومه آنلاین @endif</h2>
          <p>@if ($eng) A online resume in the form of an applicaton hosted on Github pages and built with Vuejs @else یک رزومه آنلاین به شکل یک وب اپلیکیشن ساخته شده با استفاده از Vuejs و میزبانی شده روی Github pages @endif</p>
          <a href="https://myegane49.github.io/" target="_blank">@if ($eng) Visit Website @else دیدن وبسایت @endif</a>
        </div>
        <img src="{{asset('images/resume.png')}}">
      </div>

      <div class="work">
        <div>
          <h2>@if ($eng) Contractors Map @else نقشه پیمانکاران @endif</h2>
          <p>@if ($eng) A small application for a piping company that shows different oil companies and their related contractors on the map built with Vuejs @else یک اپ کوچک برای نشان دادن شرکت ها و پیمانکارهای طرف قرار داد یک شرکت لوله سازی بر روی نقشه ایران با استفاده از Vuejs @endif</p>
          <a href="https://myegane49.xyz/contractors-map/" target="_blank">@if ($eng) Visit Website @else دیدن وبسایت @endif</a>
        </div>
        <img src="{{asset('images/contractor-map.png')}}">
      </div>

      <div class="work">
        <div>
          <h2>@if ($eng) XO Game @else بازی ضربدر دایره @endif</h2>
          <p>@if ($eng) A two player game build with vanilla Javascript and using the MVC architecture model @else یک بازی دو نفره با استفاده از Javascript و مدل معماری MVC @endif</p>
          <a href="https://myegane49.xyz/xo-game/" target="_blank">@if ($eng) Visit Website @else دیدن وبسایت @endif</a>
        </div>
        <img src="{{asset('images/xo-game.png')}}">
      </div>
    </div>
  </div>
@endsection
