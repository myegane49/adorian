require('./bootstrap');

const phoneMenu = document.getElementById('phone-menu')
const mainMenu = document.getElementById('main-menu')
const backdrop = document.getElementById('backdrop')
const closeMainMenuBtn = document.getElementById('close-btn')

const closePhoneMenu = () => {
  mainMenu.style.transform = 'translateX(100%)'
  backdrop.style.display = 'none'
}

phoneMenu.addEventListener('click', () => {
  mainMenu.style.transform = 'translateX(0)'
  backdrop.style.display = 'block'
})

closeMainMenuBtn.addEventListener('click', closePhoneMenu)
backdrop.addEventListener('click', closePhoneMenu)
