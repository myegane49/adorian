<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class PageController extends Controller
{
    public function home(Request $request, $english = '') {
        $langCookie = $request->cookie('lang');
        $langQuery = $request->query('lang');
        
        if ($langQuery == 'en') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('home', ['english' => 'en']);
        } 
        if ($langQuery == 'fa') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('home', ['english' => '']);
        }

        if ($langCookie == 'en' && $english == 'en') {
            return view('home', ['eng' => true]);
        }
        if ($langCookie == 'fa' && !$english) {
            return view('home', ['eng' => false]);
        }
        if ($langCookie == 'en' && !$english) {
            return redirect()->route('home', ['english' => 'en']);
        }
        if ($langCookie == 'fa' && $english == 'en') {
            return redirect()->route('home', ['english' => '']);
        }

        return view('home', ['eng' => false]);
    }

    public function contact(Request $request, $english = '') {
        $langCookie = $request->cookie('lang');
        $langQuery = $request->query('lang');
        
        if ($langQuery == 'en') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('contact', ['english' => 'en']);
        } 
        if ($langQuery == 'fa') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('contact', ['english' => '']);
        }

        if ($langCookie == 'en' && $english == 'en') {
            return view('contact', ['eng' => true]);
        }
        if ($langCookie == 'fa' && !$english) {
            return view('contact', ['eng' => false]);
        }
        if ($langCookie == 'en' && !$english) {
            return redirect()->route('contact', ['english' => 'en']);
        }
        if ($langCookie == 'fa' && $english == 'en') {
            return redirect()->route('contact', ['english' => '']);
        }

        return view('contact', ['eng' => false]);
    }

    public function about(Request $request, $english = '') {
        $langCookie = $request->cookie('lang');
        $langQuery = $request->query('lang');
        
        if ($langQuery == 'en') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('about', ['english' => 'en']);
        } 
        if ($langQuery == 'fa') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('about', ['english' => '']);
        }

        if ($langCookie == 'en' && $english == 'en') {
            return view('about', ['eng' => true]);
        }
        if ($langCookie == 'fa' && !$english) {
            return view('about', ['eng' => false]);
        }
        if ($langCookie == 'en' && !$english) {
            return redirect()->route('about', ['english' => 'en']);
        }
        if ($langCookie == 'fa' && $english == 'en') {
            return redirect()->route('about', ['english' => '']);
        }

        return view('about', ['eng' => false]);
    }

    public function portfolio(Request $request, $english = '') {
        $langCookie = $request->cookie('lang');
        $langQuery = $request->query('lang');
        
        if ($langQuery == 'en') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('portfolio', ['english' => 'en']);
        } 
        if ($langQuery == 'fa') {
            Cookie::queue('lang', $langQuery, null, null, null, null, true);
            return redirect()->route('portfolio', ['english' => '']);
        }

        if ($langCookie == 'en' && $english == 'en') {
            return view('portfolio', ['eng' => true]);
        }
        if ($langCookie == 'fa' && !$english) {
            return view('portfolio', ['eng' => false]);
        }
        if ($langCookie == 'en' && !$english) {
            return redirect()->route('portfolio', ['english' => 'en']);
        }
        if ($langCookie == 'fa' && $english == 'en') {
            return redirect()->route('portfolio', ['english' => '']);
        }

        return view('portfolio', ['eng' => false]);
    }
}
