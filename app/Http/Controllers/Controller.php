<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Message;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function contactForm(Request $request, $english = '') {
        if ($english) {
            $messages = [
                'name.required' => 'Your name is required',
                'email.required' => 'Email address is required',
                'email.email' => 'You must enter an Email address',
                'message.required' => 'Message is required',
            ];
        } else {
            $messages = [
                'name.required' => 'نام و نام خانوادگی الزامی است',
                'email.required' => 'ایمیل الزامی است',
                'email.email' => 'لطفا یک ایمیل معتبر وارد کنید',
                'message.required' => 'پیام الزامی است'
            ];
        }
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'message' => ['required']
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Mail::to('m.yegane49@protonmail.com')->send(new Message($request->all()));
        if ($english) {
            return back()->with('sentMessage', 'Your message has been sent');
        }
        if (!$english) {
            return back()->with('sentMessage', 'پیام شما ارسال شد');
        }
    }
}
