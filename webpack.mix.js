const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js');

mix.sass('resources/sass/layout.scss', 'public/css');
mix.sass('resources/sass/layout-en.scss', 'public/css');
mix.sass('resources/sass/pages/home.scss', 'public/css');
mix.sass('resources/sass/pages/contact.scss', 'public/css');
mix.sass('resources/sass/pages/about.scss', 'public/css');
mix.sass('resources/sass/pages/portfolio.scss', 'public/css');
